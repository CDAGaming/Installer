package me.deftware.emc.installer.ui;

import com.google.gson.*;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import me.deftware.emc.installer.Main;
import me.deftware.emc.installer.utils.AristoisInstaller;
import me.deftware.emc.installer.utils.OptiFineInstaller;
import me.deftware.emc.installer.utils.Utils;
import me.deftware.emc.installer.utils.WebUtils;
import me.deftware.emc.installer.utils.jsonbuilder.AbstractJsonBuilder;
import me.deftware.emc.installer.utils.jsonbuilder.LegacyJsonBuilder;
import me.deftware.emc.installer.utils.jsonbuilder.ModernJsonBuilder;
import me.deftware.emc.installer.utils.jsonbuilder.SubsystemJsonBuilder;
import me.deftware.emc.installer.utils.jsonbuilder.marketplace.MarketplaceElement;
import org.apache.commons.io.FileUtils;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

public class InstallerUI {

    public static String launcherDir, minecraftDir;
    private final String buildVersion = "1.5.5";
    // Settings
    private String clientName = "Aristois", modLoaderString = "None", launcherString = "Vanilla";
    private ArrayList<String> bundleDataNames = new ArrayList<>();
    private HashMap<String, String> jsonDataSet = new HashMap<>();
    private boolean errored = false;

    // Json-Related Objects
    private JsonObject emcJson, aristoisJson, marketplaceJson;
    private ArrayList<MarketplaceElement> marketplaceElements = new ArrayList<>();

    // Components
    private JButton installButton;
    private JButton cancelButton;
    private JComboBox<String> mcVersionComboBox;
    private JPanel mainPanel;
    private JLabel madeByLabel;
    private JComboBox<String> launcherComboBox;
    private JComboBox<String> modLoaderComboBox;
    private JTextField bundleDataBox;
    private JButton selectFromListButton;

    public InstallerUI(JsonObject emcJson, JsonObject aristoisJson, JsonObject marketplaceJson, JFrame frame) {
        madeByLabel.setText(madeByLabel.getText() + ", v" + buildVersion);
        frame.setTitle(clientName + " Installer");
        this.emcJson = emcJson;
        this.aristoisJson = aristoisJson;
        this.marketplaceJson = marketplaceJson;
        installButton.addActionListener((e) -> install());
        selectFromListButton.addActionListener((e -> openModuleSelector()));
        cancelButton.addActionListener((e) -> System.exit(0));
        mcVersionComboBox.addActionListener((e -> checkModLoaderSupport()));
        launcherComboBox.addActionListener(e -> syncLauncherString());
        modLoaderComboBox.addActionListener(e -> syncModLoaderString());

        // TODO: Find a MUCH better way to do this
        bundleDataBox.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                syncBundleData();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                syncBundleData();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                syncBundleData();
            }
        });
        emcJson.entrySet().forEach((entry) -> mcVersionComboBox.addItem(entry.getKey()));
    }

    public static JFrame create(JsonObject emcJson, JsonObject aristoisJson, JsonObject marketplaceJson) {
        JFrame frame = new JFrame("");
        InstallerUI ui = new InstallerUI(emcJson, aristoisJson, marketplaceJson, frame);
        JPanel panel = ui.mainPanel;
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setMinimumSize(new Dimension(400, 0));
        frame.setResizable(false);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation(dim.width / 2 - frame.getSize().width / 2, dim.height / 2 - frame.getSize().height / 2);
        return frame;
    }

    private void openModuleSelector() {
        ArrayList<String> optionList = new ArrayList<>();
        optionList.add("marketplace");

        if (marketplaceJson != null) {
            System.out.println("Checking for Addons...");
            System.out.println("Last Updated: " + (marketplaceJson.has("updated") ? marketplaceJson.get("updated").getAsString() : "N/A"));

            if (marketplaceJson.has("mods")) {
                System.out.println("Addons Located, Syncing Data...");

                for (JsonElement modElement : marketplaceJson.get("mods").getAsJsonArray()) {
                    if (modElement.getAsJsonObject() != null) {
                        MarketplaceElement dataElement = new MarketplaceElement(modElement.getAsJsonObject());

                        if (dataElement.isValid && !marketplaceElements.contains(dataElement)) {
                            System.out.println("Adding Addon to List: " + dataElement.name);

                            optionList.add(dataElement.name);
                            marketplaceElements.add(dataElement);
                        }
                    }
                }
            }
        }

        ModuleSelectorUI moduleSelector = new ModuleSelectorUI("Available Modules (Select All that Apply): ", "Select Modules to Add", optionList, marketplaceElements, bundleDataNames, true, (e) -> {
            if (e != null && !e.isEmpty()) {
                for (String bundleCheckString : e) {
                    if (!bundleDataNames.contains(bundleCheckString)) {
                        bundleDataNames.add(bundleCheckString);
                    }
                }

                StringBuilder queuedText = new StringBuilder();

                for (String bundleData : bundleDataNames) {
                    queuedText.append(bundleData).append(",");
                }

                bundleDataBox.setText(Main.trimEnd(queuedText.toString(), ','));
            }
        });

        moduleSelector.setAlwaysOnTop(true);
        moduleSelector.setVisible(true);
    }

    private void syncModLoaderString() {
        if (modLoaderComboBox.getSelectedItem() != null) {
            modLoaderString = modLoaderComboBox.getSelectedItem().toString();
        }
    }

    private void syncBundleData() {
        String bundleText = Main.trimEnd(bundleDataBox.getText(), ',');

        if (!bundleText.isEmpty()) {
            bundleDataNames.clear();
            bundleDataNames.addAll(Arrays.asList(bundleText.contains(",") ? bundleText.split(",") : new String[]{bundleText}));
        } else {
            bundleDataNames = new ArrayList<>();
        }
    }

    private void syncLauncherString() {
        if (launcherComboBox.getSelectedItem() != null) {
            launcherString = launcherComboBox.getSelectedItem().toString();
        }
    }

    private void addBundleData(String dataString, String prefix, String bundleName, String bundleVersion) {
        try {
            int responseCode = Main.getResponseCode(dataString);
            if (responseCode == HttpURLConnection.HTTP_OK) {
                String dataMavenValue = (prefix != null && !prefix.isEmpty() ? prefix + ":" : "") + bundleName + (bundleVersion != null && !bundleVersion.isEmpty() ? ":" + bundleVersion : "");
                jsonDataSet.put(dataMavenValue, dataString);
                System.out.println("Queued Bundle Package: " + dataMavenValue + " from " + dataString);
            } else {
                System.out.println("Invalid Bundle: " + bundleName + " (HTML Error Code: " + responseCode + "), Skipping...");
            }
        } catch (IOException ex) {
            System.out.println("Invalid Bundle: " + bundleName + " (HTML Error Code: IOException), Skipping...");
            ex.printStackTrace();
        }
    }

    private void checkModLoaderSupport() {
        if (mcVersionComboBox.getSelectedItem() != null) {
            String mcVersion = mcVersionComboBox.getSelectedItem().toString();
            JsonObject selectVersionData = emcJson.get(mcVersion).getAsJsonObject();
            JsonElement launchersElement = selectVersionData.get("launchers");
            JsonElement modLoadersElement = selectVersionData.get("modloaders");

            // Clear ComboBoxes before Checking
            launcherComboBox.removeAllItems();
            modLoaderComboBox.removeAllItems();

            if (launchersElement != null) {
                JsonArray supportedLaunchers = launchersElement.getAsJsonArray();
                supportedLaunchers.forEach(jsonElement -> launcherComboBox.addItem(jsonElement.getAsString()));
            }
            if (modLoadersElement != null) {
                JsonArray supportedModLoaders = modLoadersElement.getAsJsonArray();
                supportedModLoaders.forEach(jsonElement -> modLoaderComboBox.addItem(jsonElement.getAsString()));
            }

            launcherComboBox.setEnabled(launchersElement != null && launcherComboBox.getItemCount() > 0);
            launcherComboBox.setVisible(launcherComboBox.isEnabled());
            modLoaderComboBox.setEnabled(modLoadersElement != null && modLoaderComboBox.getItemCount() > 0);
            modLoaderComboBox.setVisible(modLoaderComboBox.isEnabled());

            if (modLoaderComboBox.isEnabled()) {
                // Add a None Option and Select it by Default
                modLoaderComboBox.addItem("None");
                modLoaderComboBox.setSelectedItem("None");
            } else {
                modLoaderString = "None";
            }

            if (launcherComboBox.isEnabled()) {
                // Set Vanilla to default Selection for Launcher
                launcherComboBox.setSelectedItem("Vanilla");
            }
        }
    }

    private void install() {
        // Clear and Ensure Data is set before Beginning
        launcherDir = null;
        minecraftDir = null;
        errored = false;

        launcherString = launcherComboBox.isEnabled() && launcherComboBox.getSelectedItem() != null ? launcherComboBox.getSelectedItem().toString() : "Vanilla";
        modLoaderString = modLoaderComboBox.isEnabled() && modLoaderComboBox.getSelectedItem() != null ? modLoaderComboBox.getSelectedItem().toString() : "None";

        String mcVersion = mcVersionComboBox.getSelectedItem() != null ? mcVersionComboBox.getSelectedItem().toString() : "";
        JsonObject selectVersionData = emcJson.get(mcVersion).getAsJsonObject();
        JsonObject selectedAristoisData = aristoisJson.get(mcVersion).getAsJsonObject();

        // Check Data in Bundles, remove those that are incompatible
        if (bundleDataNames != null && !bundleDataNames.isEmpty()) {
            for (String originalBundleName : bundleDataNames) {
                String bundleName = originalBundleName;
                String prefix = "me.deftware";
                String bundleVersion = "latest";
                String dataString;

                if (bundleName.contains(":")) {
                    String[] splitVersion = bundleName.split(":", 3);

                    if (splitVersion.length == 2 && !splitVersion[1].isEmpty()) {
                        bundleName = bundleName.replace(":" + splitVersion[1], "");
                        bundleVersion = splitVersion[1];
                    } else if (splitVersion.length == 3 && !splitVersion[2].isEmpty()) {
                        prefix = splitVersion[0];
                        bundleName = splitVersion[1];
                        bundleVersion = splitVersion[2];
                    }
                }

                dataString = Main.dataRawURL + "/" + prefix.replace(".", "/") + "/" + bundleName + "/" + bundleVersion + "/" + bundleName + "-" + bundleVersion + ".jar";

                // Marketplace Parsing
                MarketplaceElement dataElement = new MarketplaceElement(bundleName, marketplaceElements);

                if (dataElement.isValid) {
                    prefix = "marketplace.plugins";
                    bundleVersion = Float.toString(dataElement.version);
                    dataString = dataElement.link;
                }

                addBundleData(dataString, prefix, bundleName, bundleVersion);
            }
        }

        System.out.println("Optifine Attached in Aristois for " + mcVersion + ": " + selectedAristoisData.has("optifine"));
        JsonObject optifineData = selectedAristoisData.has("optifine") ? selectedAristoisData.get("optifine").getAsJsonObject() : null;

        String theEmcVersion = Main.customEMC.equals("") ? selectVersionData.get("version").getAsString() : Main.customEMC,
                aristoisVersion = Main.customAristois.equals("") ? selectedAristoisData.get("version").getAsString() : Main.customAristois;
        final String tweakerSuffix;
        if (optifineData != null && optifineData.get("available").getAsBoolean() && optifineData.get("first").getAsBoolean()) {
            tweakerSuffix = "Second";
            System.out.println("Using second tweaker");
        } else if (modLoaderString.equalsIgnoreCase("rift")) {
            tweakerSuffix = "NoArgs";
            System.out.println("Rift ModLoader in Use, using NoArgs Tweaker");
        } else {
            tweakerSuffix = "";
        }

        // Extra Launcher Directory Setup
        if (launcherString.equalsIgnoreCase("multimc")) {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            fileChooser.setDialogTitle("Select MultiMC Instance Directory to Install to: ");
            int returnVal = fileChooser.showOpenDialog(installButton);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                launcherDir = fileChooser.getSelectedFile().getAbsolutePath() + File.separator;
                minecraftDir = launcherDir + ".minecraft" + File.separator;
                System.out.println("MultiMC Instance Dir: " + launcherDir);
            } else {
                DialogUI multiMCError = new DialogUI("Please Set a Valid MultiMC Instance Directory to Install " + clientName + " for MultiMC.", "Error", true, () -> {
                });
                multiMCError.setAlwaysOnTop(true);
                multiMCError.setVisible(true);
                errored = true;
            }
        } else {
            // Default Method, if No other Special Cases
            try {
                launcherDir = Utils.getMinecraftRoot();
                minecraftDir = launcherDir;
            } catch (Exception ex) {
                DialogUI noVersion = new DialogUI("Cannot find Minecraft, is it installed?", "Error", true, () -> {
                });
                noVersion.setAlwaysOnTop(true);
                noVersion.setVisible(true);
            }
        }

        DialogUI dialog = new DialogUI("Installing " + clientName + ", please wait... This might take a while, do not close this window.", "", false, () -> {
            String doneString = "Install complete, the installer will now exit. Open your launcher and select ";
            if (modLoaderString.equalsIgnoreCase("None") && launcherString.equalsIgnoreCase("vanilla")) {
                doneString += String.format("\"release %s-%s\"", mcVersion, clientName);
            } else {
                doneString += "your modloader's " + "(" + modLoaderString + ") profile to start";
            }
            DialogUI installed = new DialogUI(doneString, "Install complete", true, () -> System.exit(0));
            installed.setAlwaysOnTop(true);
            installed.setVisible(true);
        });
        Thread installThread = new Thread(() -> {
            try {
                new Thread(() -> dialog.setVisible(true)).start();

                // Ensure Directories are Created
                File EMC_LIBS = new File(Utils.getMinecraftRoot() + "libraries" + File.separator + "EMC" + File.separator + mcVersion + File.separator);
                if (!EMC_LIBS.mkdirs()) {
                    System.out.println("Unable to create EMC libs dir, probably already exists");
                }

                // Download required libs
                // Execute Special Instructions for Forge
                if (modLoaderString.equalsIgnoreCase("forge")) {
                    System.out.println("Installing EMC for Forge...");
                    // Download and install EMC
                    String link = Main.dataRawURL + "/me/deftware/EMC-Forge/"
                            + theEmcVersion + "/EMC-Forge-" + theEmcVersion + "-full.jar";
                    if (!new File(minecraftDir + "mods" + File.separator + mcVersion + File.separator).mkdirs()) {
                        System.out.println("Unable to create MC Mods Directory, probably already exists...");
                    }
                    WebUtils.download(link, minecraftDir + "mods" + File.separator + mcVersion
                            + File.separator + "EMC.jar");
                    // Install Aristois
                    AristoisInstaller.installAristois(mcVersion, aristoisVersion);
                } else {
                    // Client json
                    String emcVersion = theEmcVersion;
                    if (optifineData != null && optifineData.get("available").getAsBoolean() && optifineData.get("first").getAsBoolean()) {
                        emcVersion = emcVersion.split("-")[0] + "-OF-" + emcVersion.split("-")[1];
                    }
                    AbstractJsonBuilder jsonBuilder = selectVersionData.get("subsystem").getAsString() != null && !selectVersionData.get("subsystem").getAsString().equals("") ? new SubsystemJsonBuilder(selectVersionData) : mcVersion.equals("1.12.2") ? new LegacyJsonBuilder(selectVersionData) : new ModernJsonBuilder(selectVersionData);
                    JsonObject json;
                    if (selectVersionData.get("subsystem").getAsString() != null && !selectVersionData.get("subsystem").getAsString().equals("")) {
                        System.out.println("Using subsystem json builder...");
                        json = jsonBuilder.build(mcVersion, emcVersion, launcherString, modLoaderString, selectVersionData.get("subsystem").getAsString(), clientName, selectVersionData.get("tweaker").getAsString() + tweakerSuffix, selectVersionData.get("inheritsFrom").getAsString());
                    } else {
                        json = jsonBuilder.build(mcVersion, emcVersion, launcherString, modLoaderString, clientName, selectVersionData.get("tweaker").getAsString() + tweakerSuffix, selectVersionData.get("inheritsFrom").getAsString());
                    }

                    // Add extra libs
                    JsonObject extraLibs = selectVersionData.get("extraLibs").getAsJsonObject();
                    JsonObject finalJson = json;
                    extraLibs.entrySet().forEach((entry) -> {
                        JsonObject lib = extraLibs.get(entry.getKey()).getAsJsonObject();
                        if (launcherString.equalsIgnoreCase("vanilla")) {
                            // Add lib
                            JsonArray libs = finalJson.get("libraries").getAsJsonArray();
                            libs.add(jsonBuilder.generateMavenRepo("name", lib.get("name").getAsString(), "url", lib.get("url").getAsString()));
                            // Add tweaker
                            if (!lib.get("tweaker").getAsString().equals("")) {
                                if (jsonBuilder instanceof LegacyJsonBuilder) {
                                    String minecraftArguments = finalJson.get("minecraftArguments").getAsString();
                                    finalJson.addProperty("minecraftArguments", minecraftArguments + " --tweakClass " + lib.get("tweaker").getAsString());
                                } else {
                                    JsonArray game = finalJson.get("arguments").getAsJsonObject().get("game").getAsJsonArray();
                                    game.add("--tweakClass");
                                    game.add(lib.get("tweaker").getAsString());
                                }
                            }
                        } else if (launcherString.equalsIgnoreCase("multimc")) {
                            // Add lib
                            JsonArray libs = finalJson.get("+libraries").getAsJsonArray();
                            libs.add(jsonBuilder.generateMavenRepo("name", lib.get("name").getAsString(), "url", lib.get("url").getAsString()));
                            // Add tweaker
                            if (!lib.get("tweaker").getAsString().equals("")) {
                                JsonArray game = finalJson.get("+tweakers").getAsJsonArray();
                                game.add(lib.get("tweaker").getAsString());
                            }
                        }
                    });

                    // Install Bundle Data
                    if (jsonDataSet != null && !jsonDataSet.isEmpty()) {
                        for (String dataKey : jsonDataSet.keySet()) {
                            String dataValue = jsonDataSet.get(dataKey);

                            // Install depending on Launcher
                            System.out.println("Attempting Install of " + dataKey + " for " + launcherString + "...");
                            System.out.println("Source of " + dataKey + ": " + dataValue);

                            String[] splitUrl = dataValue.split("/");
                            File queuedLocation = new File(EMC_LIBS.getAbsolutePath() + File.separator + splitUrl[splitUrl.length - 1]);
                            if (queuedLocation.exists() && !queuedLocation.delete()) {
                                System.out.println("Unable to delete previous version of " + dataKey);
                            }

                            FileUtils.copyURLToFile(new URL(dataValue), queuedLocation);
                            System.out.println("Installed " + dataKey + " for " + launcherString + " from " + dataValue + "!");
                        }
                    }

                    // OptiFine
                    if (modLoaderString.equalsIgnoreCase("None") && optifineData != null && optifineData.get("available").getAsBoolean() && Main.optifine) {
                        OptiFineInstaller.installOptifine(optifineData.get("version").getAsString(), mcVersion);

                        if (launcherString.equalsIgnoreCase("vanilla")) {
                            // Add lib
                            JsonArray libs = json.get("libraries").getAsJsonArray();
                            String optifine = optifineData.get("version").getAsString().split("iFine_")[1];
                            libs.add(jsonBuilder.generateMavenRepo("name", "optifine:OptiFine:" + optifine, "", ""));
                            // Add Tweaker
                            if (jsonBuilder instanceof LegacyJsonBuilder) {
                                String minecraftArguments = json.get("minecraftArguments").getAsString();
                                json.addProperty("minecraftArguments", minecraftArguments + " --tweakClass " + "optifine.OptiFineForgeTweaker");
                            } else {
                                if (optifineData.get("available").getAsBoolean() && optifineData.get("first").getAsBoolean()) {
                                    JsonArray game = json.get("arguments").getAsJsonObject().get("game").getAsJsonArray();
                                    game.remove(0);
                                    game.remove(0);
                                    game.add("--tweakClass");
                                    game.add("optifine.OptiFineTweaker");
                                    game.add("--tweakClass");
                                    game.add(selectVersionData.get("tweaker").getAsString() + tweakerSuffix);
                                } else {
                                    JsonArray game = json.get("arguments").getAsJsonObject().get("game").getAsJsonArray();
                                    game.add("--tweakClass");
                                    game.add("optifine.OptiFineForgeTweaker");
                                }
                            }
                            // Update date
                            String date = AbstractJsonBuilder.formatDate(new Date());
                            json.add("time", new JsonPrimitive(date));
                            json.add("releaseTime", new JsonPrimitive(date));
                        } else if (launcherString.equalsIgnoreCase("multimc")) {
                            // Add lib
                            JsonArray libs = json.get("+libraries").getAsJsonArray();
                            String optifine = optifineData.get("version").getAsString().split("iFine_")[1];
                            libs.add(jsonBuilder.generateMavenRepo("name", "optifine:OptiFine:" + optifine, "", ""));
                            // Add tweaker
                            JsonArray game = finalJson.get("+tweakers").getAsJsonArray();
                            game.add("optifine.OptiFineForgeTweaker");
                        }
                    }

                    if (launcherString.equalsIgnoreCase("vanilla")) {
                        // Install client json to a new version directory
                        File clientDir = new File(minecraftDir + "versions" + File.separator + mcVersion + "-" + clientName + File.separator);
                        if (!clientDir.mkdirs()) {
                            System.out.println("Unable to create client dir, probably already exists");
                        }
                        try (Writer writer = new FileWriter(new File(clientDir.getAbsolutePath() + File.separator + mcVersion + "-" + clientName + ".json"))) {
                            new GsonBuilder().setPrettyPrinting().create().toJson(json, writer);
                        }
                    } else if (launcherString.equalsIgnoreCase("multimc")) {
                        // Install client json to MultiMC Instance Directory
                        File patchesDir = new File(launcherDir + "patches" + File.separator);
                        if (!patchesDir.mkdirs()) {
                            System.out.println("Unable to create MultiMC Instance Patches dir, probably already exists");
                        }
                        try (Writer writer = new FileWriter(patchesDir.getAbsolutePath() + File.separator + "me.deftware.json")) {
                            new GsonBuilder().setPrettyPrinting().create().toJson(json, writer);
                        }
                    }

                    // Install Aristois
                    AristoisInstaller.installAristois(mcVersion, aristoisVersion);

                    if (launcherString.equalsIgnoreCase("vanilla")) {
                        // Install launcher profile if installing on Vanilla
                        File profiles_json = new File(minecraftDir + "launcher_profiles.json");
                        JsonObject launcherJson = new JsonParser().parse(Files.newBufferedReader(profiles_json.toPath())).getAsJsonObject();
                        AbstractJsonBuilder.removeOldAristoisEntries(launcherJson, launcherString);
                        JsonObject profiles = launcherJson.get("profiles").getAsJsonObject();
                        profiles.add(clientName, AbstractJsonBuilder.generateLaunchProfile(clientName, mcVersion, launcherString));
                        launcherJson.addProperty("selectedProfile", clientName);
                        try (Writer writer = new FileWriter(profiles_json)) {
                            new GsonBuilder().setPrettyPrinting().create().toJson(launcherJson, writer);
                        }
                    } else if (launcherString.equalsIgnoreCase("multimc")) {
                        // Install Patch to mmc-pack.json if on MultiMC
                        File pack_json = new File(launcherDir + "mmc-pack.json");
                        JsonObject packJson = new JsonParser().parse(Files.newBufferedReader(pack_json.toPath())).getAsJsonObject();
                        AbstractJsonBuilder.removeOldAristoisEntries(packJson, launcherString);
                        JsonArray components = packJson.get("components").getAsJsonArray();
                        components.add(AbstractJsonBuilder.generateLaunchProfile(clientName, mcVersion, launcherString));
                        try (Writer writer = new FileWriter(pack_json)) {
                            new GsonBuilder().setPrettyPrinting().create().toJson(packJson, writer);
                        }
                    }
                }
                dialog.onContinue();
            } catch (Exception ex) {
                ex.printStackTrace();
                DialogUI noForge = new DialogUI("Installation failed.", "Error", true, () -> dialog.setVisible(false));
                noForge.setAlwaysOnTop(true);
                noForge.setVisible(true);
            }
        });

        // Only do these Events if Launcher Errors Have not Occurred ( Do not go further if they have)
        if (!errored) {
            if (launcherString.equalsIgnoreCase("vanilla") && !(new File(minecraftDir + "versions" + File.separator + selectVersionData.get("inheritsFrom").getAsString() + File.separator + selectVersionData.get("inheritsFrom").getAsString() + ".jar").exists())) {
                DialogUI noVersion = new DialogUI("Please open your mc launcher and run vanilla mc " + selectVersionData.get("inheritsFrom").getAsString() + " once then click install again.", "Error", true, () -> {
                });
                noVersion.setAlwaysOnTop(true);
                noVersion.setVisible(true);
            } else {
                installThread.start();
            }
        }
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayoutManager(9, 2, new Insets(0, 0, 0, 0), -1, -1));
        installButton = new JButton();
        installButton.setEnabled(true);
        installButton.setText("Install");
        mainPanel.add(installButton, new GridConstraints(6, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        cancelButton = new JButton();
        cancelButton.setEnabled(true);
        cancelButton.setText("Cancel");
        mainPanel.add(cancelButton, new GridConstraints(6, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        mcVersionComboBox = new JComboBox();
        final DefaultComboBoxModel defaultComboBoxModel1 = new DefaultComboBoxModel();
        mcVersionComboBox.setModel(defaultComboBoxModel1);
        mainPanel.add(mcVersionComboBox, new GridConstraints(1, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        mainPanel.add(spacer1, new GridConstraints(7, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final JLabel label1 = new JLabel();
        label1.setText("Select Launcher and ModLoader to Install to:");
        mainPanel.add(label1, new GridConstraints(4, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer2 = new Spacer();
        mainPanel.add(spacer2, new GridConstraints(7, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final JLabel label2 = new JLabel();
        label2.setText("Select which Minecraft version you want to install for:");
        mainPanel.add(label2, new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        madeByLabel = new JLabel();
        madeByLabel.setText("Made by https://deftware.me/");
        mainPanel.add(madeByLabel, new GridConstraints(8, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        launcherComboBox = new JComboBox();
        final DefaultComboBoxModel defaultComboBoxModel2 = new DefaultComboBoxModel();
        launcherComboBox.setModel(defaultComboBoxModel2);
        mainPanel.add(launcherComboBox, new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        modLoaderComboBox = new JComboBox();
        modLoaderComboBox.setEnabled(true);
        mainPanel.add(modLoaderComboBox, new GridConstraints(5, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        bundleDataBox = new JTextField();
        bundleDataBox.setText("");
        bundleDataBox.setToolTipText("Example: Type \"marketplace\" to install the EMC Marketplace");
        mainPanel.add(bundleDataBox, new GridConstraints(3, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        selectFromListButton = new JButton();
        selectFromListButton.setText("Select From List");
        mainPanel.add(selectFromListButton, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label3 = new JLabel();
        label3.setText("Add extra modules to install:");
        label3.setToolTipText("Modules are Seperated by Commas");
        mainPanel.add(label3, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return mainPanel;
    }

}
