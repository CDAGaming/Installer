package me.deftware.emc.installer.ui;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import me.deftware.emc.installer.utils.Utils;
import me.deftware.emc.installer.utils.jsonbuilder.marketplace.MarketplaceElement;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

public class ModuleSelectorUI extends JDialog {

    private JPanel contentPane;
    private JButton buttonOK;
    private JLabel modalText;
    private JScrollPane checkboxScroller;
    private JPanel checkBoxPanel;
    private JButton buttonSelectAll;
    private JButton buttonSelectNone;
    private DialogCallback cb;

    private ArrayList<String> selectedOptions = new ArrayList<>();
    private ArrayList<JCheckBox> checkers = new ArrayList<>();

    public ModuleSelectorUI(String text, String title, ArrayList<String> options, ArrayList<MarketplaceElement> marketplaceElements, ArrayList<String> preSelected, boolean continueButton, DialogCallback cb) {
        this.cb = cb;
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        buttonOK.addActionListener((e) -> onContinue());
        buttonSelectAll.addActionListener((e -> selectAll()));
        buttonSelectNone.addActionListener((e -> deSelectAll()));
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                onContinue();
            }
        });
        buttonOK.setVisible(continueButton);
        setTitle(title);
        modalText.setText(text);
        pack();
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setMinimumSize(new Dimension(400, 0));
        setResizable(false);
        setLocation(dim.width / 2 - getSize().width / 2, dim.height / 2 - getSize().height / 2);

        if (!options.isEmpty()) {
            for (String option : options) {
                JCheckBox checkBox = new JCheckBox(option);
                checkBox.setHorizontalAlignment(SwingConstants.LEFT);
                checkBox.setVerticalAlignment(SwingConstants.TOP);

                // Add data to checkbox if is valid marketplace data
                if (!marketplaceElements.isEmpty()) {
                    MarketplaceElement marketplaceElement = new MarketplaceElement(option, marketplaceElements);

                    if (marketplaceElement.isValid) {
                        checkBox.setToolTipText("<html>Description: " + Utils.formatToHtml(marketplaceElement.description) + "<br><br>" + "Author: " + Utils.formatToHtml(marketplaceElement.author) + "<br>" + "Version: " + marketplaceElement.version + "</html>");
                    }
                }

                // Sync already selected options
                // TODO: Rewrite some areas to get this to sync properly
                /*if (!selectedOptions.isEmpty() && selectedOptions.contains(option)) {
                    checkBox.doClick();
                    addToSelectedOptions(checkBox);
                }*/

                checkers.add(checkBox);
            }
        }

        if (!checkers.isEmpty()) {
            Box box = Box.createVerticalBox();
            for (JCheckBox checkbox : checkers) {
                checkbox.addActionListener((e -> syncSelection(checkbox)));
                box.add(checkbox);
            }
            checkBoxPanel.revalidate();
            checkBoxPanel.repaint();
            checkboxScroller.setViewportView(box);
        }
    }

    private void syncSelection(JCheckBox checkBox) {
        if (checkBox != null) {
            if (checkBox.isSelected() && !selectedOptions.contains(checkBox.getText())) {
                addToSelectedOptions(checkBox);
            } else if (!selectedOptions.isEmpty() && selectedOptions.contains(checkBox.getText())) {
                removeFromSelectedOptions(checkBox);
            }
        }
    }

    private void addToSelectedOptions(JCheckBox checkBox) {
        if (!selectedOptions.contains(checkBox.getText())) {
            selectedOptions.add(checkBox.getText());
        }
    }

    private void removeFromSelectedOptions(JCheckBox checkBox) {
        selectedOptions.remove(checkBox.getText());
    }

    private void selectAll() {
        if (checkers != null && !checkers.isEmpty()) {
            for (JCheckBox checkBox : checkers) {
                checkBox.setSelected(true);
                addToSelectedOptions(checkBox);
            }
        }
    }

    private void deSelectAll() {
        if (checkers != null && !checkers.isEmpty()) {
            for (JCheckBox checkBox : checkers) {
                checkBox.setSelected(false);
                removeFromSelectedOptions(checkBox);
            }
        }
    }

    public void onContinue() {
        if (cb != null) {
            cb.onCallback(selectedOptions);
        }
        dispose();
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        contentPane = new JPanel();
        contentPane.setLayout(new GridLayoutManager(2, 1, new Insets(10, 10, 10, 10), -1, -1));
        contentPane.setPreferredSize(new Dimension(335, 335));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(1, 4, new Insets(0, 0, 0, 0), -1, -1));
        contentPane.add(panel1, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, 1, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        panel1.add(spacer1, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel1.add(panel2, new GridConstraints(0, 3, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        buttonOK = new JButton();
        buttonOK.setText("Continue");
        panel2.add(buttonOK, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        buttonSelectAll = new JButton();
        buttonSelectAll.setText("Select All");
        panel1.add(buttonSelectAll, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        buttonSelectNone = new JButton();
        buttonSelectNone.setText("Select None");
        panel1.add(buttonSelectNone, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel3 = new JPanel();
        panel3.setLayout(new GridLayoutManager(2, 2, new Insets(0, 0, 0, 0), -1, -1));
        contentPane.add(panel3, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        modalText = new JLabel();
        modalText.setText("Available Modules (Select All that Apply to you):");
        panel3.add(modalText, new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer2 = new Spacer();
        panel3.add(spacer2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        checkboxScroller = new JScrollPane();
        panel3.add(checkboxScroller, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        checkBoxPanel = new JPanel();
        checkBoxPanel.setLayout(new GridLayoutManager(2, 2, new Insets(0, 0, 0, 0), -1, -1));
        checkboxScroller.setViewportView(checkBoxPanel);
        final Spacer spacer3 = new Spacer();
        checkBoxPanel.add(spacer3, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final Spacer spacer4 = new Spacer();
        checkBoxPanel.add(spacer4, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return contentPane;
    }

    @FunctionalInterface
    public interface DialogCallback {

        void onCallback(ArrayList<String> selectedOptions);

    }

}
