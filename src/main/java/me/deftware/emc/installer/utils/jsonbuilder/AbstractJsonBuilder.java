package me.deftware.emc.installer.utils.jsonbuilder;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

public abstract class AbstractJsonBuilder {

    protected JsonObject emcJson;

    public AbstractJsonBuilder(JsonObject emcJson) {
        this.emcJson = emcJson;
    }

    public static JsonObject generateLaunchProfile(String name, String mcVersion, String launcherString) {
        JsonObject json = new JsonObject();
        if (launcherString.equalsIgnoreCase("vanilla")) {
            json.add("name", new JsonPrimitive(name));
            json.add("type", new JsonPrimitive("custom"));
            json.add("created", new JsonPrimitive(formatDateMs(new Date())));
            json.add("lastUsed", new JsonPrimitive(formatDateMs(new Date())));
            json.add("icon", new JsonPrimitive("Diamond_Block"));
            json.add("lastVersionId", new JsonPrimitive(mcVersion + "-" + name));
        } else if (launcherString.equalsIgnoreCase("multimc")) {
            json.add("cachedName", new JsonPrimitive(name));
            json.add("cachedVersion", new JsonPrimitive(mcVersion + "-" + name));
            json.add("uid", new JsonPrimitive("me.deftware"));
        }
        return json;
    }

    public static JsonObject removeOldAristoisEntries(JsonObject data, String launcherString) {
        // TODO: Maybe Slim this down
        ArrayList<String> thingsToRemove = new ArrayList<>();
        ArrayList<JsonElement> elementsToRemove = new ArrayList<>();

        if (launcherString.equalsIgnoreCase("vanilla")) {
            JsonObject profiles = data.get("profiles").getAsJsonObject();
            for (Map.Entry<String, JsonElement> entry : profiles.entrySet()) {
                if (entry.getKey().toLowerCase().contains("aristois")) {
                    System.out.println("Removing old invalid Aristois profile " + entry.getKey());
                    thingsToRemove.add(entry.getKey());
                }
            }
            thingsToRemove.forEach(entry -> profiles.remove(entry));
            if (profiles.has("Aristois")) {
                System.out.println("Removing old default Aristois profile");
                profiles.remove("Aristois");
            }
        } else if (launcherString.equalsIgnoreCase("multimc")) {
            JsonArray components = data.get("components").getAsJsonArray();
            for (JsonElement componentObj : components) {
                if (componentObj != null) {
                    JsonObject component = componentObj.getAsJsonObject();
                    if (component != null && component.get("cachedName").getAsString().equalsIgnoreCase("Aristois")) {
                        System.out.println("Removing old Aristois Patch Listing " + component.get("cachedName").getAsString());
                        elementsToRemove.add(componentObj);
                    }
                }
            }
            elementsToRemove.forEach(jsonElement -> components.remove(jsonElement));
        }
        return data;
    }

    public static String formatDateMs(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss.SSS'Z'");
        return dateFormat.format(date);
    }

    public static String formatDate(Date date) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
            return dateFormat.format(date);
        } catch (Exception e) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            return dateFormat.format(date);
        }
    }

    public abstract JsonObject build(String mcVersion, String emcVersion, String launcherString, String modLoaderString, String id, String emcTweaker, String inheritsFrom);

    public abstract JsonObject build(String mcVersion, String emcVersion, String launcherString, String modLoaderString, String subsystemVersion, String id, String emcTweaker, String inheritsFrom);

    public JsonObject generateMavenRepo(String name, String n, String url, String u) {
        JsonObject obj = new JsonObject();
        obj.addProperty(name, n);
        if (!url.equals("")) {
            obj.addProperty(url, u);
        }
        return obj;
    }

}
