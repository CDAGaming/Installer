package me.deftware.emc.installer.utils;

import me.deftware.emc.installer.ui.DialogUI;

import javax.net.ssl.HttpsURLConnection;
import javax.swing.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

@SuppressWarnings("Duplicates")
public class AristoisInstaller {

    private static final boolean donorBuild = false;

    public static void installAristois(String mcVersion, String aristoisVersion) throws Exception {
        String oauthCode = "";
        if (donorBuild) {
            oauthCode = String.valueOf(JOptionPane.showInputDialog("Please enter your 6 digit oAuth code (Join srv.mc-oauth.net with your Minecraft client): "));
        }
        System.out.println("Installing Aristois v" + aristoisVersion);
        File EMC_LIBS = new File(Utils.getMinecraftRoot() + "libraries" + File.separator + "EMC" + File.separator + mcVersion + File.separator);
        File aristois = new File(EMC_LIBS.getAbsolutePath() + File.separator + "Aristois.jar");
        if (aristois.exists() && !aristois.delete()) {
            System.out.println("Unable to delete previous version of Aristois");
        }
        String downloadOutput = download("https://aristois.net/maven/download", mcVersion + "/" + aristoisVersion + "/" + (donorBuild ? "donor.jar" : "free.jar"), oauthCode, EMC_LIBS.getAbsolutePath() + File.separator + "Aristois.jar");
        if (!downloadOutput.equals("")) {
            DialogUI error = new DialogUI(downloadOutput + " The installer will now exit.", "Error", true, () -> {
                System.exit(0);
            });
            error.setAlwaysOnTop(true);
            error.setVisible(true);
        }
        System.out.println("Installed Aristois");
    }

    public static String download(String uri, String file, String token, String fileName) throws Exception {
        URL url = new URL(uri);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36");
        connection.setRequestProperty("token", token);
        connection.setRequestProperty("file", file);
        connection.setRequestMethod("GET");
        FileOutputStream out = new FileOutputStream(fileName);
        InputStream in = connection.getInputStream();
        int read;
        byte[] buffer = new byte[4096];
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
        in.close();
        out.close();
        if (new File(fileName).length() < 150) {
            return readFile(new File(fileName));
        }
        return "";
    }

    public static String readFile(File f) throws IOException {
        StringBuilder output = new StringBuilder();
        for (String s : Files.readAllLines(Paths.get(f.getAbsolutePath()), StandardCharsets.UTF_8)) {
            output.append(s);
        }
        return output.toString();
    }

}
