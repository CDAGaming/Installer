package me.deftware.emc.installer.utils.jsonbuilder;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import me.deftware.emc.installer.ui.InstallerUI;
import me.deftware.emc.installer.utils.WebUtils;

import java.io.File;
import java.util.Date;

@SuppressWarnings("Duplicates")
public class SubsystemJsonBuilder extends AbstractJsonBuilder {

    public SubsystemJsonBuilder(JsonObject emcJson) {
        super(emcJson);
    }

    @Override
    public JsonObject build(String mcVersion, String emcVersion, String launcherString, String modLoaderString, String id, String mainClass, String inheritsFrom) {
        String mappings = emcJson.get("mappings").getAsString();
        String date = formatDate(new Date());
        JsonObject jsonObject = new JsonObject();
        // Properties
        jsonObject.add("inheritsFrom", new JsonPrimitive(inheritsFrom));
        jsonObject.add("id", new JsonPrimitive(mcVersion + "-" + id));
        jsonObject.add("time", new JsonPrimitive(date));
        jsonObject.add("releaseTime", new JsonPrimitive(date));
        jsonObject.add("type", new JsonPrimitive("release"));
        // Main class
        JsonObject arguments = new JsonObject();
        arguments.add("game", new JsonArray());
        jsonObject.add("arguments", arguments);
        jsonObject.add("mainClass", new JsonPrimitive(emcJson.get("mainClass").getAsString()));
        // Libraries
        JsonArray libsArray = new JsonArray();
        libsArray.add(generateMavenRepo("name", "net.fabricmc:sponge-mixin:0.7.11.16", "url",
                "https://maven.fabricmc.net/"));
        libsArray.add(generateMavenRepo("name", "net.fabricmc:tiny-mappings-parser:0.1.0.6", "url",
                "https://maven.fabricmc.net/"));
        libsArray.add(generateMavenRepo("name", "net.fabricmc:tiny-remapper:0.1.0.23", "url",
                "https://maven.fabricmc.net/"));
        libsArray.add(generateMavenRepo("name", "net.fabricmc:fabric-loader-sat4j:2.3.5.4", "url",
                "https://maven.fabricmc.net/"));
        libsArray.add(generateMavenRepo("name", "com.google.jimfs:jimfs:1.1", "url",
                "https://maven.fabricmc.net/"));
        libsArray.add(generateMavenRepo("name", "org.ow2.asm:asm:7.0", "url",
                "https://maven.fabricmc.net/"));
        libsArray.add(generateMavenRepo("name", "org.ow2.asm:asm-analysis:7.0", "url",
                "https://maven.fabricmc.net/"));
        libsArray.add(generateMavenRepo("name", "org.ow2.asm:asm-commons:7.0", "url",
                "https://maven.fabricmc.net/"));
        libsArray.add(generateMavenRepo("name", "org.ow2.asm:asm-tree:7.0", "url",
                "https://maven.fabricmc.net/"));
        libsArray.add(generateMavenRepo("name", "org.ow2.asm:asm-util:7.0", "url",
                "https://maven.fabricmc.net/"));
        libsArray.add(generateMavenRepo("name", "net.fabricmc:yarn:" + mappings, "url",
                "https://maven.fabricmc.net/"));
        if (modLoaderString.equalsIgnoreCase("fabric")) {
            // Fabric ModLoader Dependencies
            libsArray.add(generateMavenRepo("name", "net.fabricmc:fabric-loader:0.4.7+build.147", "url",
                    "https://maven.fabricmc.net/"));
            try {
                installEMCToMods(mcVersion, emcVersion);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            libsArray.add(generateMavenRepo("name", "me.deftware:EMC-F:" + emcVersion, "url",
                    "https://gitlab.com/EMC-Framework/maven/raw/master/"));
            libsArray.add(generateMavenRepo("name", "me.deftware:subsystem:latest-separate", "url",
                    "https://gitlab.com/EMC-Framework/maven/raw/master/"));
        }
        jsonObject.add("libraries", libsArray);
        return jsonObject;
    }

    @Override
    public JsonObject build(String mcVersion, String emcVersion, String launcherString, String modLoaderString, String subsystemVersion, String id, String emcTweaker, String inheritsFrom) {
        String mappings = emcJson.get("mappings").getAsString();
        String date = formatDate(new Date());
        JsonObject jsonObject = new JsonObject();
        JsonArray libsArray = new JsonArray();

        if (launcherString.equalsIgnoreCase("vanilla")) {
            // Properties
            jsonObject.add("inheritsFrom", new JsonPrimitive(inheritsFrom));
            jsonObject.add("id", new JsonPrimitive(mcVersion + "-" + id));
            jsonObject.add("time", new JsonPrimitive(date));
            jsonObject.add("releaseTime", new JsonPrimitive(date));
            jsonObject.add("type", new JsonPrimitive("release"));
            // Main class
            JsonObject arguments = new JsonObject();
            arguments.add("game", new JsonArray());
            jsonObject.add("arguments", arguments);
            jsonObject.add("mainClass", new JsonPrimitive(emcJson.get("mainClass").getAsString()));
        } else if (launcherString.equalsIgnoreCase("multimc")) {
            jsonObject.add("formatVersion", new JsonPrimitive(1));

            JsonArray tweakerArray = new JsonArray();
            jsonObject.add("+tweakers", tweakerArray);
        }

        // Libraries
        libsArray.add(generateMavenRepo("name", "net.fabricmc:sponge-mixin:0.7.11.30", "url",
                "https://maven.fabricmc.net/"));
        libsArray.add(generateMavenRepo("name", "net.fabricmc:tiny-mappings-parser:0.1.1.8", "url",
                "https://maven.fabricmc.net/"));
        libsArray.add(generateMavenRepo("name", "net.fabricmc:tiny-remapper:0.1.0.34", "url",
                "https://maven.fabricmc.net/"));
        libsArray.add(generateMavenRepo("name", "net.fabricmc:fabric-loader-sat4j:2.3.5.4", "url",
                "https://maven.fabricmc.net/"));
        libsArray.add(generateMavenRepo("name", "com.google.jimfs:jimfs:1.1", "url",
                "https://maven.fabricmc.net/"));
        libsArray.add(generateMavenRepo("name", "org.ow2.asm:asm:7.1", "url",
                "https://maven.fabricmc.net/"));
        libsArray.add(generateMavenRepo("name", "org.ow2.asm:asm-analysis:7.1", "url",
                "https://maven.fabricmc.net/"));
        libsArray.add(generateMavenRepo("name", "org.ow2.asm:asm-commons:7.1", "url",
                "https://maven.fabricmc.net/"));
        libsArray.add(generateMavenRepo("name", "org.ow2.asm:asm-tree:7.1", "url",
                "https://maven.fabricmc.net/"));
        libsArray.add(generateMavenRepo("name", "org.ow2.asm:asm-util:7.1", "url",
                "https://maven.fabricmc.net/"));
        libsArray.add(generateMavenRepo("name", "net.fabricmc:yarn:" + mappings, "url",
                "https://maven.fabricmc.net/"));
        if (modLoaderString.equalsIgnoreCase("fabric")) {
            // Fabric ModLoader Dependencies
            libsArray.add(generateMavenRepo("name", "net.fabricmc:fabric-loader:0.4.7+build.147", "url",
                    "https://maven.fabricmc.net/"));
            try {
                installEMCToMods(mcVersion, emcVersion);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            libsArray.add(generateMavenRepo("name", "me.deftware:EMC-F:" + emcVersion, "url",
                    "https://gitlab.com/EMC-Framework/maven/raw/master/"));
            libsArray.add(generateMavenRepo("name", "me.deftware:subsystem:" + subsystemVersion, "url",
                    "https://gitlab.com/EMC-Framework/maven/raw/master/"));
        }

        if (launcherString.equalsIgnoreCase("vanilla")) {
            jsonObject.add("libraries", libsArray);
        } else if (launcherString.equalsIgnoreCase("multimc")) {
            jsonObject.add("+libraries", libsArray);
        }

        // Extra Properties (Or Other Properties Required by Other Launchers)
        if (launcherString.equalsIgnoreCase("multimc")) {
            jsonObject.add("mainClass", new JsonPrimitive(emcJson.get("mainClass").getAsString()));
            jsonObject.add("name", new JsonPrimitive(id));
            jsonObject.add("releaseTime", new JsonPrimitive(date));

            JsonArray requiresArray = new JsonArray();
            JsonObject requiresData = new JsonObject();

            requiresData.add("equals", new JsonPrimitive(mcVersion));
            requiresData.add("uid", new JsonPrimitive("net.minecraft"));
            requiresArray.add(requiresData);
            jsonObject.add("requires", requiresArray);

            jsonObject.add("uid", new JsonPrimitive("me.deftware"));
            jsonObject.add("version", new JsonPrimitive(emcVersion));
        }
        return jsonObject;
    }

    public void installEMCToMods(String mcVersion, String version) throws Exception {
        System.out.println("Installing EMC for " + mcVersion);
        File MODS_FOLDER = new File(InstallerUI.minecraftDir + "mods");

        if (MODS_FOLDER.exists() || MODS_FOLDER.mkdirs()) {
            WebUtils.download("https://gitlab.com/EMC-Framework/maven/raw/master/me/deftware/EMC-F" + "/" + version + "/EMC-F-" + version + ".jar", MODS_FOLDER.getAbsolutePath() + File.separator + "EMC.jar");
            System.out.println("Installed EMC for " + mcVersion);
        } else {
            System.out.println("Failed to add to or create mods directory, probably already exists or something weird happened");
        }
    }

}
