package me.deftware.emc.installer.utils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Set;

public class Utils {

    public static String getMinecraftRoot() throws Exception {
        if (OSUtils.isWindows()) {
            return System.getenv("APPDATA") + File.separator + ".minecraft" + File.separator;
        } else if (OSUtils.isLinux()) {
            return System.getProperty("user.home") + File.separator + ".minecraft" + File.separator;
        } else if (OSUtils.isMac()) {
            return System.getProperty("user.home") + File.separator + "Library" + File.separator + "Application Support"
                    + File.separator + "minecraft" + File.separator;
        }
        throw new RuntimeException("Unable to find Minecraft version");
    }

    /**
     * Search for a specified JsonElement within a Json File
     *
     * @param jsonFile     Json File Data
     * @param searchTarget Element Name to look for
     * @return The Matching JsonElement found in the Json File
     */
    public static JsonElement lookupElementInJson(File jsonFile, String searchTarget) {
        try {
            return lookupElementInJson(FileUtils.readFileToString(jsonFile, StandardCharsets.UTF_8), searchTarget);
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Retrieves Json Data as a JsonObject
     *
     * @param jsonFile Json File Location
     * @return a JsonObject derived from Json Data
     */
    public static JsonObject getJsonDataAsObject(File jsonFile) {
        try {
            return getJsonDataAsObject(FileUtils.readFileToString(jsonFile, StandardCharsets.UTF_8));
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Retrieves Json Data as a JsonObject
     *
     * @param jsonData Json Data
     * @return a JsonObject derived from Json Data
     */
    public static JsonObject getJsonDataAsObject(String jsonData) {
        return new Gson().fromJson(jsonData, JsonObject.class);
    }

    /**
     * Search for a specified JsonElement within a Json File
     *
     * @param jsonFileData Json File Data
     * @param searchTarget Element Name to look for
     * @return The Matching JsonElement found in the Json File
     */
    public static JsonElement lookupElementInJson(String jsonFileData, String searchTarget) {
        JsonElement resultingElement = null;
        JsonObject jsonData;

        try {
            if (!jsonFileData.isEmpty()) {
                jsonData = getJsonDataAsObject(jsonFileData);

                if (jsonData != null) {
                    Set<Map.Entry<String, JsonElement>> entries = jsonData.entrySet();
                    for (Map.Entry<String, JsonElement> entry : entries) {
                        if (entry.getKey().contains(searchTarget)) {
                            resultingElement = entry.getValue();
                        }
                    }
                } else {
                    System.out.println("Json Data returned null, looking for " + searchTarget);
                }
            } else {
                System.out.println("Json File Data is invalid, please correct your parameters!");
            }
        } catch (Exception ex) {
            System.out.println("Failed to lookup " + searchTarget + " in json file...");
            ex.printStackTrace();
            ;
        }

        return resultingElement;
    }

    public static String formatToHtml(String original) {
        return original.replaceAll("\\n", "<br>");
    }
}
